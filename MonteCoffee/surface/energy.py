r"""
Module: surface/energy.py

Contains the bookkeeping functions, data to organize work with a simple 
nearest neighbor Voter's reaction site. The reaction site is depicted in 
the freely available chapter
https://public.lanl.gov/afv/Voter2007-KMCchapter.pdf
on page 11.

In this implementation, the neighborhood can be depicted in a following cartoon
  
  6789
  4if5
  0123 
  
Here, the letters "i" and "f" mark initial and final atomic sites.
Numbers 0,1,2,3,4,5,6,7,8,9 show the positions of neighbors in the 
Voter's rection site. There are 2^10 possibilities for occupation of the 
sites if single adsorbate type is assumed.

"""

import numpy as np
#from numba import jit


def get_n2occ(ntype):
    """ Defined here in order not to repeat in the code.
        ntype : Neighborhood type, integer number. ntype could assume values
        0,1,2...,1023.
    Returns:
    --------
        n2occ: Array defining occupations of atomic sites in a reaction site.
        It is basically binary representation of the ntype.
    """
    return np.array([(ntype // t) % 2 for t in n2pown])


mjsigma2 = np.array([[0,-1], [1,0]]) # -j * sigma2, where sigma2 is 
# the second Pauli matrix. It is used to rotate 2d vector anti-clockwise.

# Relative coordinates of the neigbors in terms of basis vectors vl and vp
# vl -- final_position - intial_position, 2D vector
# vp -- vector made by anti-clockwise rotation of vl by right angle
# The origin is at the atomic site "0". 
rel_coo2 = np.array( ((0,0),(1,0),(2,0),(3,0),(0,1),(3,1),(0,2),(1,2),(2,2),(3,2)) )

n2pown = np.array([2**n for n in range(10)])

def get_n2ij(sid2ij, shape, s, f):
    """
    The function finds the list of the neighboring sites (lattice) coordinates. 
    sid2ij : bookkeeping array site_ID -> integer coordinates of sites (i,j) 
    shape : size of the lattice in the first and second direction
    s : start site ID
    f : final site ID 
    This function is defined here in such form in order to accelerate with numba.
    """
    
    ij = sid2ij[s]      # coordinates of the diffusing site
    vl = sid2ij[f] - ij # vector along the diffusion direction
    
    #print(ij, vl, vl-shape, shape)
    
    vp = np.dot(mjsigma2, vl) # vector perpendicular to the diffusion direction

    aux = ij-vl-vp
    n2ij = np.zeros((10,2), dtype=int)

    for e, (l,p) in enumerate(rel_coo2):
        i,j = np.remainder( aux + vl * l + vp * p, shape)
        n2ij[e,:] = (i,j)

    return n2ij

def get_ntype(sid2ij, ij2occ, shape, si, sf):
    """ Determine the type of the neighborhood (ntype) """
    n2ij = get_n2ij( sid2ij, shape, si, sf )
    return (n2pown * ij2occ[n2ij[:,0], n2ij[:,1]]).sum()

def get_rate_ntype(ntype2af, ntype2eb_Ha, T_Ha, ntype):
    """ Arrenius law """
    return ntype2af[ntype] * np.exp( -ntype2eb_Ha[ntype] / T_Ha )
