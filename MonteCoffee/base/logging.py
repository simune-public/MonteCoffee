r"""
Module: logging.py
Log class used to log the results of a kMC run. 

"""
import time

class tc:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    
class Log:

    def __init__(self,parameters):
        r"""Constructor for Log objects.
            
            Method initializes a filename based on the CPU date 
            and time.
    
            Parameters
            ----------
            parameters : dict 
                parameters to dump at the beginning of a log.
                For example 'dict = {'T':300,'pCO':1E2}'

            Returns
            -------
            Log instance

        """
        self.fn = 'kMClog_'+time.strftime('%Y-%m-%d_%H:%M')+'.txt'

        with open(self.fn, 'a') as f:
            f.write(r"""  __  __             _        ____       __  __           
|  \/  | ___  _ __ | |_ ___ / ___|___  / _|/ _| ___  ___ 
| |\/| |/ _ \| '_ \| __/ _ \ |   / _ \| |_| |_ / _ \/ _ \
| |  | | (_) | | | | ||  __/ |__| (_) |  _|  _|  __/  __/
|_|  |_|\___/|_| |_|\__\___|\____\___/|_| |_|  \___|\___|
                                                         
""")
            f.write('\nMikkel Jorgensen\nChalmers University of Technology\nGoteborg, Sweden\n2018')
            f.write('\n'+'-'*80+'\n')
            f.write('Simulation parameters\n')
            for p in parameters.keys():
                f.write(format(str(p),'<10')+format(':','<5')+
                        str(parameters[p])+'\n')
            
            
            f.write('\n'+'-'*80+'\n'*3)
            f.write('kinetic Monte Carlo Log \n\n')
            f.write('{:<10s} {:^20s} {:^30s} {:<10s}'.format('Step',
                    'time[hr:min:s]','Sim time [s]','Events called (ordered as in user_events.py)\n'))

            

            
    
    def write_line(self,string):
        r"""
        Appends a string to the end of the log.

        """
        with open(self.fn, 'a') as f:
            f.write(string)

    def dump_point(self, step, sim_time,ev_called):
        r"""Adds a simulation point to the log. 
           
           Method writes the Monte Carlo step number 'step',
           the time in the MC simulation'sim_time',
           and the number of event calls 'ev_called'.
       

        Parameters
            ----------
            step : int 
                step number
            sim_time : float
                the simulation time
            ev_called : list of int
                the number of times events are called,
                for example [N_CO_ads,N_CO_des,...]

        """
        with open(self.fn, 'a') as f:
            time_str = time.strftime('%H:%M:%S')
            f.write('{:<10s} {:^20s} {:^30s} {:<10s}'.format(str(step),
                    time_str,str(sim_time),str(["%.0f"%item for item in ev_called])+'\n'))


