from __future__ import print_function

import sys, numpy as np

from MonteCoffee.base.kmc_vssm import KMC_VSSM

class UserKMC(KMC_VSSM):

    def __init__(self, system, parameters={}):
        r"""Constructor for NeighborKMC objects.
            
            Method calls constructor of NeighborKMCSurface. 
    
            Parameters
            ----------
            system : System instance
                A system instance with defined neighborlists.

            parameters : dict
                Parameters used, which are dumped to the log file.
                Example: parameters = 
                {'T':700,'Note':'Test simulation'}

            Returns
            -------
            NeighborKMC instance
        """
        import ase.units
        
        dp = parameters
        self.load_events(dp)
        system.update_ij2occ()
        KMC_VSSM.__init__(self, system=system, parameters=dp)
        self.sqr_dmax = min( np.array(system.shape) // 2 )**2-1

        self.step_end = dp['step_end'] if 'step_end' in dp else 1000
        self.tend = dp['tend'] if 'tend' in dp else None
        self.datafile = dp['datafile'] if 'datafile' in dp else 'kmc.dat'
        self.trajectory = dp['trajectory'] if 'trajectory' in dp else None
        
        af = self.events[0].ntype2af[0]
        ea = self.events[0].ntype2eb[0]*ase.units.Hartree
        T = dp['T']
        print('Attempt frequency {:10.8e} (s^-1)'.format( af))
        print('Activation energy {:10.8e} (eV)'.format( ea ))
        print('Temperature {:10.8e} (K)'.format( T ))
        print('Recommended end time, tend {:10.8e} (s)'.format( \
            0.25 / (af * np.exp(-ea * 11604.58857702 / T )) * 1000) )

    def load_events(self, parameters):
        r"""Loads the events list.
    
            User-overridden method.
            
            Method loads the event list 'self.events' which is used to
            keep track of event-types in the simualtion.
    
            Parameters
            ----------
            parameters : dict
                The parameters to pass to the simulation events.

        """
        import pyclbr
        import user_events as ue
        
        self.reverses = {} # Reverse steps # NEEDED IN THE PARENT CLASS, WHY NOT INITIALIZING ALSO THERE?
        self.events = []
        classes = pyclbr.readmodule("user_events")
        event_names = []
        line_nrs = []
        for c in classes:
            if classes[c].file.endswith("user_events.py"):
                event_names.append(c)
                line_nrs.append(classes[c].lineno)

        # Sort events by line number:
        event_names_srt = [event_names[n] for n in np.argsort(line_nrs)] 
        for n in event_names_srt:            
            exec("self.events.append(ue."+n+"(parameters))")

        # Track which steps are considered each others inverse.
        for i in range(len(self.events)): 
            if self.events[i].rev is not None:
                self.reverses[i] = self.events[i].rev

        self.evs_exec = np.zeros(len(self.events))

                
    def run_kmc(self):
        r"""Runs a kmc simulation.

            Returns
            -------
            0 if simulation is finished.

        """
        import ase.io
        from timeit import default_timer as timer
        
        step = 0                  # Initialize the main step counter

        if self.verbose: 
            print('\nRunning simulation...')

        if self.trajectory is not None:
            _a = self.system.get_ase_atoms()
            ase.io.Trajectory(self.trajectory, mode='w', atoms=_a).write()

        df = open( self.datafile, 'w')

        shape = self.system.shape
        cci = np.array(np.where(self.system.ij2occ>0)).T
        fmt = "{: 5d} {: 10.4e} {: 7d} \n"

        while self.t < self.tend:
            self.frm_step()
            
            cc = np.array(np.where(self.system.ij2occ>0)).T
            sqr_dist = (abs(cc - cci)**2).sum()
            if sqr_dist>self.sqr_dmax: 
                break

            if step % self.LogSteps == 0:
                df.write( fmt.format(step, self.t, sqr_dist) )
                if self.verbose:
                    #print(self.rs[np.where(self.rs>0.0)[0]])
                    print("{:9d} {:10.4e} {}".format(step, self.t, sqr_dist))

            if step % self.SaveSteps == 0 and self.trajectory is not None: #Save every self.SaveSteps steps.
                _a = self.system.get_ase_atoms()
                ase.io.Trajectory(self.trajectory, mode='a', atoms=_a).write()

            step+=1


        if self.verbose:
            print("{:9d} {:10.4e} {}".format(step, self.t, sqr_dist))
        df.write( fmt.format(step, self.t, sqr_dist) )
        df.close()

        return 0
       
